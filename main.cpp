#include "LM75B.h" // Temperature sensor library for mbed
#include "MMA7660.h" // Accelerometer sensor library for mbed
#include "loopmqClient.h" // MQTT client library
#include "MQTTEthernet.h" // Library to connect to the internet using ethernet
#include "C12832.h" // LCD library with generic interface
#include "rtos.h"

// Configuration values needed to connect to Loop Cloud
#define ORG "liveiot.mq"             // For a registered connection, replace with your org
#define ID "mbed"                        // For a registered connection, replace with your id
#define AUTH_TOKEN "8Hgw*j#978pT"                // For a registered connection, replace with your auth-token
#define TYPE DEFAULT_TYPE_NAME       // For a registered connection, replace with your type
    
#define MQTT_PORT 1883                // Define the port to connect to
#define MQTT_TLS_PORT 8883          
#define Loop_IOT_PORT MQTT_PORT

#define MQTT_MAX_PACKET_SIZE 250        // Define the size of the packet to be transferred

#if defined(TARGET_UBLOX_C027)
#warning "Compiling for mbed "
#include "mbed.h"
#elif defined(TARGET_LPC1768)
#warning "Compiling for mbed LPC1768"
#include "LPC1768.h"
#elif defined(TARGET_K64F)
#warning "Compiling for mbed K64F"
#include "K64F.h"
#endif

Serial pc(USBTX, USBRX); // tx, rx
DigitalOut myled3(LED3);
DigitalOut myled2(LED2);
DigitalOut myled4(LED4);
DigitalOut myled1(LED1);
    
bool quickstartMode = true;
char org[16] = ORG;         
char type[30] = TYPE;               
char id[30] = ID;                 // mac without colons
char auth_token[30] = AUTH_TOKEN; // Auth_token is only used in non-quickstart mode

bool connected = false;
bool mqttConnecting = false;
bool netConnected = false;
bool netConnecting = false;
bool ethernetInitialising = true;
int connack_rc = 0; // MQTT connack return code
int retryAttempt = 0;
int menuItem = 0;

char* joystickPos = "CENTRE";
int blink_interval = 0;

char* ip_addr = "";
char* gateway_addr = "";
char* host_addr = "";
int connectTimeout = 1000;

char var[16];                                 // data published on the topic

int num=0;                                    // intermediate variable for the counter function

/*
Function to generate a counter to be published on a particular topic
*/
char *counter(){
    while(1){
    if(num < 100 ){    
      num++;
    //  delay(10);
    sprintf(var,"%d",num);
   return var;
    }
    else if (num == 100)
      num = 0;
    } 
}

void off()
{
    myled3 =1;
}

void red()
{
    myled2 =1;
}

void yellow()
{ 
    myled4 =1;
}

void green()
{ 
    myled1 =1;
}


void flashing_yellow(void const *args)
{
    bool on = false;
    while (!connected && connack_rc != MQTT_NOT_AUTHORIZED && connack_rc != MQTT_BAD_USERNAME_OR_PASSWORD) {  // flashing yellow only while connecting
        on = !on;
        if (on)
            yellow();
        else
            off();
        wait(0.5);
    }
}


void flashing_red(void const *args)  // to be used when the connection is lost
{
    bool on = false;
    while (!connected) {
        on = !on;
        if (on)
            red();
        else
            off();
        wait(2.0);
    }
}

///////////////////Connect to the broker over internet/////////////////
int connect(MQTT::Client<MQTTEthernet, Countdown, MQTT_MAX_PACKET_SIZE>* client, MQTTEthernet* ipstack)
{
    const char* iot_Loop = ".litmusloop.com";

    char hostname[strlen(org) + strlen(iot_Loop) + 1];
    sprintf(hostname, "%s%s", org, iot_Loop);
    EthernetInterface& eth = ipstack->getEth();
    ip_addr = eth.getIPAddress();
    gateway_addr = eth.getGateway();

    // Construct clientId - d:org:type:id
    char clientId[strlen(org) + strlen(type) + strlen(id) + 5];
    sprintf(clientId, "d:%s:%s:%s", org, type, id);

    //Network debug statements
    LOG("=====================================\n");
    LOG("Connecting Ethernet.\n");
    LOG("IP ADDRESS: %s\n", eth.getIPAddress());
    LOG("MAC ADDRESS: %s\n", eth.getMACAddress());
    LOG("Gateway: %s\n", eth.getGateway());
    LOG("Network Mask: %s\n", eth.getNetworkMask());
    LOG("Server Hostname: %s\n", hostname);
    LOG("Client ID: %s\n", clientId);
    LOG("=====================================\n");


    netConnecting = true;
    int rc = ipstack->connect(hostname, Loop_IOT_PORT,connectTimeout);
    if (rc != 0) {
        WARN("IP Stack connect returned: %d\n", rc);
        return rc;
    }
    netConnected = true;
    netConnecting = false;

    // MQTT Connect
    
    mqttConnecting = true;
    MQTTPacket_connectData data = MQTTPacket_connectData_initializer;
    data.MQTTVersion = 3;
    data.clientID.cstring = clientId;

    if (!quickstartMode) {
        data.username.cstring = "demoroom";
        data.password.cstring = auth_token;
    }

    if ((rc = client->connect(data)) == 0) {
        connected = true;
      green();
      //displayMessage("Connected");
        wait(1);
       
    } else
        WARN("MQTT connect returned %d\n", rc);
    if (rc >= 0)
        connack_rc = rc;
    mqttConnecting = false;
    return rc;
}


int getConnTimeout(int attemptNumber)
{
    /* First 10 attempts try within 3 seconds, next 10 attempts retry after every 1 minute
     after 20 attempts, retry every 10 minutes */
    return (attemptNumber < 10) ? 3 : (attemptNumber < 20) ? 60 : 600;
}


void attemptConnect(MQTT::Client<MQTTEthernet, Countdown, MQTT_MAX_PACKET_SIZE>* client, MQTTEthernet* ipstack)
{
    connected = false;

    // make sure a cable is connected before starting to connect
    while (!linkStatus()) {
        wait(1.0f);
        WARN("Ethernet link not present. Check cable connection\n");
    }

    while (connect(client, ipstack) != MQTT_CONNECTION_ACCEPTED) {
        if (connack_rc == MQTT_NOT_AUTHORIZED || connack_rc == MQTT_BAD_USERNAME_OR_PASSWORD)
            return; // don't reattempt to connect if credentials are wrong

       Thread red_thread(flashing_red);

        int timeout = getConnTimeout(++retryAttempt);
        WARN("Retry attempt number %d waiting %d\n", retryAttempt, timeout);

        /* if ipstack and client were on the heap we could deconstruct and goto a label where they are constructed
          or maybe just add the proper members to do this disconnect and call attemptConnect(...)*/

        // this works - reset the system when the retry count gets to a threshold
        if (retryAttempt == 5)
            NVIC_SystemReset();
        else
            wait(timeout);
    }
}

///////////////Publish the data to the given topic over internet/////////////////////
int publish(MQTT::Client<MQTTEthernet, Countdown, MQTT_MAX_PACKET_SIZE>* client, MQTTEthernet* ipstack)
{
    MQTT::Message message;
    char* pubTopic = "demoroom/mbed/pub"; // Define the topic to publish and subscribe

    char buf[250]; // 
    sprintf(buf,"{\"command\":\"mbed\",\"IP Address\":\"%s\",\"data\":{\"number\":\"%s\",\"Litmus\":\"Loop\"}}",counter(),counter());
    
   /*sprintf(buf,
     "{\"d\":{\"myName\":\"IoT mbed\",\"accelX\":%0.4f,\"accelY\":%0.4f,\"accelZ\":%0.4f,\"temp\":%0.4f,\"joystick\":\"%s\",\"potentiometer1\":%0.4f,\"potentiometer2\":%0.4f}}",
            MMA.x(), MMA.y(), MMA.z(), sensor.temp(), joystickPos, ain1.read(), ain2.read());
   */         
    message.qos = MQTT::QOS0;
    message.retained = false;
    message.dup = false;
    message.payload = (void*)buf;
    message.payloadlen = strlen(buf); 

    LOG("Publishing %s\n", buf); 
    return client->publish(pubTopic, message); // publish the data
}


char* getMac(EthernetInterface& eth, char* buf, int buflen)    // Obtain MAC address
{
    strncpy(buf, eth.getMACAddress(), buflen);

    char* pos;                                                 // Remove colons from mac address
    while ((pos = strchr(buf, ':')) != NULL)
        memmove(pos, pos + 1, strlen(pos) + 1);
    return buf;
}


void messageArrived(MQTT::MessageData& md)
{
    MQTT::Message &message = md.message;
    char topic[md.topicName.lenstring.len + 1];

    sprintf(topic, "%.*s", md.topicName.lenstring.len, md.topicName.lenstring.data);

    LOG("Message arrived on topic %s: %.*s\n",  topic, message.payloadlen, message.payload);

    // Command topic: iot-2/cmd/blink/fmt/json - cmd is the string between cmd/ and /fmt/
    char* start = strstr(topic, "/cmd/") + 5;
    int len = strstr(topic, "/fmt/") - start;

    if (memcmp(start, "blink", len) == 0) {
        char payload[message.payloadlen + 1];
        sprintf(payload, "%.*s", message.payloadlen, (char*)message.payload);

        char* pos = strchr(payload, '}');
        if (pos != NULL) {
            *pos = '\0';
            if ((pos = strchr(payload, ':')) != NULL) {
                int blink_rate = atoi(pos + 1);
                blink_interval = (blink_rate <= 0) ? 0 : (blink_rate > 50 ? 1 : 50/blink_rate);
            }
        }
    } else
        WARN("Unsupported command: %.*s\n", len, start);
}


int main()
{
    quickstartMode = (strcmp(org, "quickstart") == 0);

    //lcd.set_font((unsigned char*) Arial12x12);  // Set a nice font for the LCD screen

    led2 = LED2_OFF; 
    
  //displayMessage("Connecting");
  Thread yellow_thread(flashing_yellow);
 // Thread menu_thread(menu_loop);

    LOG("***** Loop Client Ethernet Example *****\n");
    MQTTEthernet ipstack;
    ethernetInitialising = false;
    MQTT::Client<MQTTEthernet, Countdown, MQTT_MAX_PACKET_SIZE> client(ipstack);
    LOG("Ethernet Initialized\n");

    if (quickstartMode)
        getMac(ipstack.getEth(), id, sizeof(id));

    attemptConnect(&client, &ipstack);

    if (connack_rc == MQTT_NOT_AUTHORIZED || connack_rc == MQTT_BAD_USERNAME_OR_PASSWORD) {
        red();
        while (true)
            wait(1.0); // Permanent failures - don't retry
    }

    if (!quickstartMode) {
        int rc = 0;
        if ((rc = client.subscribe("iot-2/cmd/+/fmt/json", MQTT::QOS1, messageArrived)) != 0)
            WARN("rc from MQTT subscribe is %d\n", rc);
    }

    blink_interval = 0;
    int count = 0;
    while (true) {
        if (++count == 100) {
            // Publish a message every second
            if (publish(&client, &ipstack) != 0)
                attemptConnect(&client, &ipstack);   // if we have lost the connection
            count = 0;
        }

        if (blink_interval == 0)
            led2 = LED2_OFF;
        else if (count % blink_interval == 0)
            led2 = !led2;
        client.yield(10);  // allow the MQTT client to receive messages
    }
}
