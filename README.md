# mbed-Looopmq

The project demonstrates the implementation of MQTT.**Message Queue Telemetry Transport (MQTT)** is an extremely simple and lightweight messaging protocol, designed for constrained devices and low bandwidth, high latency and unreliable networks. 
The protocol uses **publish/subscribe** communication pattern and is used for machine to machine communication.It plays an important role in the internet of things. MQTT protocol works on the **TCP/IP connection**.

The library provides an example of publish and subscribe messaging with a server that supports MQTT using ***Arch pro Mbed LPC1768***.


## Overview


The library provides an example of publish and subscribe messaging with a server that supports MQTT using Archpro mbed.
 
***Features provided by the client library:***

* Connect the device to any IP network using Ethernet, Wi-Fi, 4G/LTE
* Publish any message to the MQTT server in standard JSON format on a specific topic 
* Subscribe data from the server to the device on a specific topic
* Unsubscribe the topic to no longer communicate to the device
* Disconnect the device from any network connectivity.


***The following Table shows status of the client and the server when the above functions are implemented:***

> |Function |  Server Status   |    Client Status
> ----------------|------------------|---------------
|Looopmq.connect | Connected       | Connected|
|Loopmq.publish |Connected |  Connected|
|Loopmq.subscribe | Connected | Connected|
|Loopmq.unsubscribe | Connected | Disconnected|
|Loopmq.disconnect |  Disconnected |  Disconnected|


## Getting Started With mbed LPC1768

>1. Go to [mbed.org](https://developer.mbed.org/) to login or sign up in order to get access to the mbed online compiler
2. Add device by clicking the add device on the top right corner and selecting Seedstudio Arch pro
3. Download and install the windows serial configurator from [serial](https://developer.mbed.org/handbook/Windows-serial-configuration). This can be ued to debug the code by displaying messages on the serial port
4. Import and run the [blinky ](https://developer.mbed.org/teams/mbed/code/mbed_blinky/?platform=Seeeduino-Arch-Pro) to get used to the hardware and the compiler
5. Import the loopmq library to the compiler and connect the device to ehernet
6. Open the serial port to check the messages which shows if the client is connected to the docker.

The board connections are shown in the figure below

![alt text](https://bytebucket.org/litmusloopdocs/archprombed-loopcloud/raw/master/extra/Board_config.png)


## Configuration
The user need to define a list of parameters in order to connect the device to a server in a secured manner.

**Below are the list of minimum definitions required by the user to send data to the cloud:**

```
// Configuration values needed to connect to Loop Cloud
#define ORG "loopdocker1"             // For a registered connection, replace with your org
#define ID ""                        // For a registered connection, replace with your id
#define AUTH_TOKEN ""                // For a registered connection, replace with your auth-token
#define TYPE DEFAULT_TYPE_NAME       // For a registered connection, replace with your type

```

## Functions

1.***loopmq.connect (client ID)***
This function is used to connect the device or the client to the client ID specified by the user.

```
int connect(MQTT::Client<MQTTEthernet, Countdown, MQTT_MAX_PACKET_SIZE>* client, MQTTEthernet* ipstack)
{
    const char* iot_Loop = ".cloudapp.net";

    char hostname[strlen(org) + strlen(iot_Loop) + 1];
    sprintf(hostname, "%s%s", org, iot_Loop);
    EthernetInterface& eth = ipstack->getEth();
    ip_addr = eth.getIPAddress();
    gateway_addr = eth.getGateway();
```

2.***loopmq.subscribe***

This function is used to subscribe to a topic to which data will be published from the user to the device.

```
int publish(MQTT::Client<MQTTEthernet, Countdown, MQTT_MAX_PACKET_SIZE>* client, MQTTEthernet* ipstack)
{
    MQTT::Message message;
    char* pubTopic = "mbed/loop1"; // Define the topic to publish and subscribe

    char buf[250]; // 
```

3.***loopmq.unsubscribe***

This function is used to unsubscribe the device from the server. Calling this function will stop sending messages from the device to the server.

4.***lopmq.disconnect ()***

This function is used to disconnect the device from the server. Disconnect does not stop the functionality of the device but disconnects it from the network. The device works fine locally but does not send any update to the internet.

5.***JSON PARSER***

This function is used to create a JSON payload to be passed to the broker as payload

```
sprintf(buf,"{\"command\":\"sensor\",\"number\":\"%i\",\"data\":{\"number\":\"%i\",\"Litmus\":\"Loop\"}}",var,num);
```